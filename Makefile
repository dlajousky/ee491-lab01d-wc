###############################################################################
###          University of Hawaii, College of Engineering
### @brief   Lab 02a - wc2 - EE 205 - Spr 2022
###
### @file    Makefile
### @version 2.0 - Initial version
###
### Build a Word Counting program
###
### @author  Duncan Lajousky <lajousky@hawaii.edu>
### @date    27_01_2022
###
### @see     https://www.gnu.org/software/make/manual/make.html
###############################################################################

CC     = gcc
CFLAGS = -g -Wall

TARGET = wc2

all: $(TARGET)

wc: wc2.c
	$(CC) $(CFLAGS) -o $(TARGET) wc.c

clean:
	rm -f $(TARGET)

