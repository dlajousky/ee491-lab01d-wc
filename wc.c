/////////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab01d - wc - EE 491 - Spr 2022
///
/// @file    wc.c
/// @version 1.0 - Initial version
///
/// wc - print newline, word, and byte counts for each file
///
/// @author   Duncan Lajousky <@todo lajousky@hawaii.edu>
/// @date     18_01_2022
///
/// @see     https://linuxcommand.org/lc3_man_pages/wc1.html
///
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>

int main() {
FILE * file;
char path[100];
char ch;
int chars, words, lines; //defining characters, words, lines//

printf("Enter file path");
scanf("%s", path); //reads and opens designate file for counting//

file = fopen(path, "r"); //open file//
if (file == NULL)
{
   printf("Unable to open file"); //error if can't open file//
   exit(EXIT_FAILURE);
}
chars=words=lines=0;
while ((ch=fgetc(file)) !=EOF)
{
   chars++;
   if (ch== '\n' || ch== '\0') //checks new line//
      lns++;
   if (ch== ' ' || ch== '\t' || ch== '\n' || ch== '\0') //checks words//
      words++;
}
if (chars>0) //for last line and word//
{
   words++;
   lines++;
}
printf("Characters =%d\n", chars); //printing gathered data//
printf("Words =%d\n", words);
printf("Lines =%d\n", lines);

fclose(file);
   return 0;
}
