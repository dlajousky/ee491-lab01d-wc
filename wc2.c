/////////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab02a - wc2 - EE 491 - Spr 2022
///
/// @file    wc.c
/// @version 10 - Most Stable Version
///
/// wc2 - print newline, word, and byte counts for each file
///
/// @author   Duncan Lajousky <lajousky@hawaii.edu>
/// @date     27_01_2022
///
/// @see     https://linuxcommand.org/lc3_man_pages/wc1.html
///
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#define STOP
#define YES 1
#define NO 0
int main(int argc, char* argv[]) {
int i;
for (i=0; i<=argc; i++) {
FILE* fp;
char ch;
int chars, words, lines;//defining characters, words, lines//
int wordflag=NO; //initialzing word flag for white space//
if ((fp=fopen(argv[i+1],"r")) == NULL) { //to open multiple files from cmd//
   printf("Error Opening File\n");
   exit(EXIT_FAILURE);
}
chars=words=lines=0;
while ((ch=fgetc(fp))!=EOF) {
   chars++;
   if (ch== '\n' || ch== '\0') //checks new line//
      lines++;
   if (!isspace(ch) && wordflag == NO) { //checks words and accounts for white space//
      wordflag = YES;
      words++;
   }
   if (isspace(ch) && wordflag == YES) wordflag=NO;
}
printf("%d\t%d\t%d\t%s\n", lines, words, chars, argv[i+1]);

fclose(fp);
}
return 0;
}
